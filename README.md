# LTI Tool Consumer (Example)

An example application to illustrate how LTI works.
This was designed to be used in conjunction with the LTI Tool Provider example
codebase which can be found [here](https://gitlab.com/sonofborge/lti-tool-provider).

## Overview

**TODO:** Add overview covering basic LTI, assignments

## Getting Started

1. `rbenv install`
1. `gem install bundler`
1. `bundle install`
1. `shotgun`

## Resources

**TODO:** Add list of further resources
