#
# GET Home
#
get '/' do
  erb :index
end

#
# POST Set Username
#
post '/set_username' do
  @username = params['username']
  session[:username] = @username
  redirect to '/tool_config'
end

#
# GET Tool Configuration
#
get '/tool_config' do
  unless session[:username]
    redirect to '/'
    return
  end

  @message = params['message']
  @username = session[:username]
  erb :tool_config
end

#
# POST Tool Launch
#
post '/tool_launch' do
  # Make sure the tool name, launch url, and consumer key/secrets are set
  if %w{ tool_name launch_url consumer_key consumer_secret }.any? {
      |key| params[key].nil? || params[key] == ''
    }
    redirect to '/tool_config?message=Please%20set%20all%20values'
    return
  end

  # Instantiate LTI Tool Provider Configuration
  @tool_config = IMS::LTI::ToolConfig.new(
    :title => params['tool_name'],
    :launch_url => params['launch_url']
  )

  # Set custom message
  @tool_config.set_custom_param(
    'message_from_tool_consumer',
    'Hello world! With <3 from the LTI Tool Consumer')

  # Instantiate LTI Tool Consumer w/ key/secret
  # TODO: DRY up this code (used again in POST /grade_passback)
  @consumer = IMS::LTI::ToolConsumer.new(
    params['consumer_key'],
    params['consumer_secret']
  )

  # Apply Tool Provider Configuration to Consumer
  @consumer.set_config(@tool_config)

  # Set Host URL (convenience)
  host = request.scheme + "://" + request.host_with_port

  # Set Launch Data
  # Only 'resource_link_id' is required, the rest are recommended
  # http://www.imsglobal.org/LTI/v1p1pd/ltiIMGv1p1pd.html#_Toc309649684
  @consumer.resource_link_id = "1234567890"
  @consumer.launch_presentation_return_url = host + '/tool_return'
  @consumer.lis_person_name_given = session[:username]
  @consumer.user_id = Digest::MD5.hexdigest(session[:username])
  @consumer.roles = "learner"
  @consumer.context_id = "666"
  @consumer.context_title = "Psychology 101"
  @consumer.context_label = "PSY101"
  @consumer.tool_consumer_instance_name = "WSU"
  @consumer.tool_consumer_instance_description = "Washington State University"

  if params['assignment']
    @consumer.lis_outcome_service_url = host + '/grade_passback'
    @consumer.lis_result_sourcedid = '83873872987329873264783687634'
  end

  @autolaunch = !!params['autolaunch']

  erb :tool_launch
end

#
# GET Tool Return
#
get '/tool_return' do
  @error_message = params['lti_errormsg']
  @message = params['lti_msg']
  puts "Warning: #{params['lti_errorlog']}" if params['lti_errorlog']
  puts "Info: #{params['lti_log']}" if params['lti_log']

  erb :tool_return
end

#
# POST Grade Passback
#
post '/grade_passback' do

  # Instantiate new LTI Outcome Request
  req = IMS::LTI::OutcomeRequest.from_post_request(request)

  consumer = IMS::LTI::ToolConsumer.new('consumerKey', 'consumerSecret')

  # Check to see if the request was valid
  #
  # TODO: Refactor
  if consumer.valid_request?(request)

    # Check to see if the OAuth token expired
    #
    # TODO: Refactor into its own method
    if consumer.request_oauth_timestamp.to_i - Time.now.utc.to_i > 60*60
      throw_oauth_error
    end

    # Check the nonce (number used once)
    # This isn't actually checking anything like it should, but when creating a
    # real LTI service, the nonce and timestamp are used to guard against
    # "replay attacks."
    #
    # Resource: https://hueniverse.com/beginners-guide-to-oauth-part-iii-security-architecture-e9394f5263b5
    #
    # TODO: Refactor into its own method
    if was_nonce_used_in_last_x_minutes?(consumer.request_oauth_nonce, 60)
      throw_oauth_error
    end

    # Instantiate a new LTI Outcome Response
    res = IMS::LTI::OutcomeResponse.new
    res.message_ref_identifier = req.message_identifier
    res.operation = req.operation
    res.code_major = 'success'
    res.severity = 'status'

    # Is this a replace request?
    if req.replace_request?
      res.description = "Your old score of 0 has been replaced with #{req.score}"
    # Is this a read request?
    elsif req.read_request?
      res.description = "You score is 50"
      res.score = 50
    # Is this a delete request?
    elsif req.delete_request?
      res.description = "You score has been cleared"
    # If none of the above, the request is unsupported
    else
      res.code_major = 'unsupported'
      res.severity = 'status'
      res.description = "#{req.operation} is not supported"
    end

    # Generate XML Response
    headers 'Content-Type' => 'text/xml'
    res.generate_response_xml

  # If the request wasn't valid, throw OAuth error
  else
    throw_oauth_error
  end
end
